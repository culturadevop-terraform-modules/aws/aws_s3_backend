locals {
  bucket_name = var.resources.bucket_name
  table_name  = var.resources.table_name
  tags        = try(var.resources.tags != null ? var.resources.tags : null, null)

  project = var.project

  common_tags = {
    Project     = var.project.project
    Environment = var.project.environment
    CreatedBy   = var.project.createdBy
  }
}

resource "aws_s3_bucket" "this" {
  bucket = local.bucket_name

  force_destroy = true
  lifecycle {
    prevent_destroy = false
  }

  tags = merge(local.common_tags, local.tags != null ? local.tags : {
    Name        = local.bucket_name
    Description = "Created by ${local.project.createdBy}"
  })
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.this.id

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "encryption" {
  bucket = aws_s3_bucket.this.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_dynamodb_table" "terraform_locks" {
  name           = local.table_name
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = merge(local.common_tags, local.tags != null ? local.tags : {
    Name        = local.table_name
    Description = "Created by ${local.project.createdBy}"
  })
}
