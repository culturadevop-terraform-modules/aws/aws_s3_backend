provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

module "module_usage" {
  source = "./../.."

  project = {
    project     = "my-project"
    environment = "dev"
    createdBy   = "markitos"
    group       = "example-usage-s3-backend"
  }

  resources = {
    bucket_name = "component-123456-abcdefg-markitos-bcket"
    table_name  = "component-123456-abcdefg-markitos-table"
    tags = {
      Name        = "TestingBucket S3"
      Description = "TestingBucket S3"
    }
  }
}
