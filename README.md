<!-- BEGIN_TF_DOCS -->



## Resources

The following resources are used by this module:

- [aws_dynamodb_table.terraform_locks](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) (resource)
- [aws_s3_bucket.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) (resource)
- [aws_s3_bucket_server_side_encryption_configuration.encryption](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration) (resource)
- [aws_s3_bucket_versioning.versioning](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_versioning) (resource)
```yaml
locals {
  bucket_name = var.resources.bucket_name
  table_name  = var.resources.table_name
  tags        = try(var.resources.tags != null ? var.resources.tags : null, null)

  project = var.project

  common_tags = {
    Project     = var.project.project
    Environment = var.project.environment
    CreatedBy   = var.project.createdBy
  }
}

resource "aws_s3_bucket" "this" {
  bucket = local.bucket_name

  force_destroy = true
  lifecycle {
    prevent_destroy = false
  }

  tags = merge(local.common_tags, local.tags != null ? local.tags : {
    Name        = local.bucket_name
    Description = "Created by ${local.project.createdBy}"
  })
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.this.id

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "encryption" {
  bucket = aws_s3_bucket.this.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_dynamodb_table" "terraform_locks" {
  name           = local.table_name
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = merge(local.common_tags, local.tags != null ? local.tags : {
    Name        = local.table_name
    Description = "Created by ${local.project.createdBy}"
  })
}
```

## Providers

The following providers are used by this module:

- <a name="provider_aws"></a> [aws](#provider\_aws) (~>5.35.0)
```yaml
terraform {
  required_version = "~>1.7.1"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>5.35.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "~>3.1.0"
    }
  }
}
```

## Required Inputs

The following input variables are required:

### <a name="input_project"></a> [project](#input\_project)

Description: some common settings like project name, environment, and tags for all resources-objects

Type:

```hcl
object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
```

### <a name="input_resources"></a> [resources](#input\_resources)

Description: resources of objects to create

Type:

```hcl
object({
    bucket_name = string
    table_name  = string
    tags        = optional(map(string))
  })
```

## Optional Inputs

No optional inputs.
```yaml
# #:{'..'}>----------------------------------------------
# #:{'..'}> required variables
# #:{'..'}>----------------------------------------------
variable "resources" {
  description = "resources of objects to create"
  nullable    = false
  type = object({
    bucket_name = string
    table_name  = string
    tags        = optional(map(string))
  })
  ###### BUCKET_NAME #####
  validation {
    condition     = length(var.resources.bucket_name) >= 3 && length(var.resources.bucket_name) <= 63
    error_message = "Bucket names must be between 3 (min) and 63 (max) characters long"
  }

  validation {
    condition     = can(regex("^[a-z0-9.-]{1,64}$", var.resources.bucket_name))
    error_message = "Bucket names can consist only of lowercase letters, numbers, dots (.), and hyphens (-)."
  }

  validation {
    condition     = can(regex("^[a-z0-9].*[a-z0-9]$", var.resources.bucket_name))
    error_message = "Bucket names must begin and end with a letter or number"
  }

  validation {
    condition     = !strcontains(var.resources.bucket_name, "..")
    error_message = "Bucket names must not contain two adjacent periods."
  }

  validation {
    condition     = !can(regex("^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$", var.resources.bucket_name))
    error_message = "Bucket names must not be formatted as an IP address (for example, 192.168.5.4)"
  }

  validation {
    condition     = !startswith(var.resources.bucket_name, "xn--")
    error_message = "Bucket names must not start with the prefix xn--"
  }

  validation {
    condition     = !startswith(var.resources.bucket_name, "sthree-")
    error_message = "Bucket names must not start with the prefix sthree-"
  }

  validation {
    condition     = !endswith(var.resources.bucket_name, "-s3alias")
    error_message = "Bucket names must not end with the suffix -s3alias."
  }

  validation {
    condition     = !endswith(var.resources.bucket_name, "--ol-s3")
    error_message = "Bucket names must not end with the suffix --ol-s3"
  }
  ###### END.OF BUCKET_NAME #####


  ###### TABLE_NAME #####
  validation {
    condition     = length(var.resources.table_name) >= 3 && length(var.resources.table_name) <= 63
    error_message = "Table name must be between 3 (min) and 63 (max) characters long"
  }

  validation {
    condition     = can(regex("^[a-z0-9-]{1,64}$", var.resources.table_name))
    error_message = "Table name can consist only of lowercase letters, numbers and hyphens (-)."
  }

  validation {
    condition     = can(regex("^[a-z].*[a-z]$", var.resources.table_name))
    error_message = "Table name must begin and end with a letter"
  }

  validation {
    condition     = !strcontains(var.resources.table_name, "--")
    error_message = "Table name must not contain two adjacent hyphens (-)."
  }
  ###### END.OF TABLE_NAME #####
}


variable "project" {
  description = "some common settings like project name, environment, and tags for all resources-objects"
  nullable    = false
  type = object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
}
# #:{'..'}>----------------------------------------------



# #:{'..'}>----------------------------------------------
# #:{'..'}> optional variables
# #:{'..'}>----------------------------------------------
# #:{'..'}>----------------------------------------------
```

## Outputs

The following outputs are exported:

### <a name="output_backend_bucket_arn"></a> [backend\_bucket\_arn](#output\_backend\_bucket\_arn)

Description: arn of the created bucket s3

### <a name="output_backend_bucket_id"></a> [backend\_bucket\_id](#output\_backend\_bucket\_id)

Description: Id of the created bucket s3

### <a name="output_backend_bucket_name"></a> [backend\_bucket\_name](#output\_backend\_bucket\_name)

Description: name of the created bucket s3

### <a name="output_backend_configuration_file_dot_env"></a> [backend\_configuration\_file\_dot\_env](#output\_backend\_configuration\_file\_dot\_env)

Description: generate the configuration file for the backend infrastructure

### <a name="output_backend_table_name"></a> [backend\_table\_name](#output\_backend\_table\_name)

Description: name of the storage table

### <a name="output_debug"></a> [debug](#output\_debug)

Description: For debug purpose.

### <a name="output_project_backend_configuration_file_dot_env"></a> [project\_backend\_configuration\_file\_dot\_env](#output\_project\_backend\_configuration\_file\_dot\_env)

Description: generate the project configuration file for use remote backend state

### <a name="output_project_configuration_file_dot_env"></a> [project\_configuration\_file\_dot\_env](#output\_project\_configuration\_file\_dot\_env)

Description: generate the project configuration file .env
```yaml
output "backend_bucket_id" {
  description = "Id of the created bucket s3"
  value       = aws_s3_bucket.this.id
}

output "backend_bucket_arn" {
  description = "arn of the created bucket s3"
  value       = aws_s3_bucket.this.arn
}

output "backend_bucket_name" {
  description = "name of the created bucket s3"
  value       = local.bucket_name
}

output "backend_table_name" {
  description = "name of the storage table"
  value       = aws_dynamodb_table.terraform_locks.name
}

output "backend_configuration_file_dot_env" {
  description = "generate the configuration file for the backend infrastructure"
  value       = <<EOT
TF_VAR_backend_name         = ${local.bucket_name}
TF_VAR_backend_tags         = {"Tag1":"Tag1Value"}
TF_VAR_provider_region      = "PUT_HERE_YOUR_PROVIDER_REGION"
TF_VAR_provider_profile     = "PUT_HERE_YOUR_PROVIDER_PROFILE"
TF_VAR_project_name         = ${local.project.project}
TF_VAR_project_environment  = ${local.project.environment}
TF_VAR_project_group        = ${local.project.group}
TF_VAR_project_created_by   = ${local.project.createdBy}
TF_TOKEN_gitlab_com         = "PUT_HERE_YOUR_PERSONAL_GITLAB_TOKEN"
EOT
}

output "project_backend_configuration_file_dot_env" {
  description = "generate the project configuration file for use remote backend state"
  value       = <<EOT
region              = "PUT_HERE_YOUR_PROVIDER_REGION"
profile             = "PUT_HERE_YOUR_PROVIDER_PROFILE"
bucket              = "${local.bucket_name}"
key                 = "${local.project.project}-${local.bucket_name}-${local.project.environment}.tfstate"
dynamodb_table      = "${aws_dynamodb_table.terraform_locks.name}"
encrypt             = true
TF_TOKEN_gitlab_com = "PUT_HERE_YOUR_PERSONAL_GITLAB_TOKEN"
EOT
}


output "project_configuration_file_dot_env" {
  description = "generate the project configuration file .env"
  value       = <<EOT
TF_VAR_project        = "my-project"
TF_VAR_environment    = "dev"
TF_VAR_createdBy      = "markitos"
TF_VAR_group          = "my-group"
TF_VAR_region         = "PUT_HERE_YOUR_PROVIDER_REGION"
TF_VAR_profile        = "PUT_HERE_YOUR_PROVIDER_PROFILE"
TF_TOKEN_gitlab_com   = "PUT_HERE_YOUR_PERSONAL_GITLAB_TOKEN"  
EOT
}


output "debug" {
  description = "For debug purpose."
  value       = local.bucket_name
}
```

## Example from YAML
```yaml
project:
  project: module_template
  region: eu-west-1
  profile: default
  createdBy: markitos
  environment: dev
  group: example-usage-s3-backend
resources:
  bucket_name: component-123456-abcdefg-yaml-markitos-bcket
  table_name: component-123456-abcdefg-yaml-markitos-table
  tags:
    Name: TestingBucket S3
    Description: TestingBucket S3
```

```yaml
provider "aws" {
  profile = local.manifest.project.profile
  region  = local.manifest.project.region
}

locals {
  manifest = yamldecode(file("${path.cwd}/manifest.yaml"))
}

module "module_usage" {
  source    = "./../.."
  project   = local.manifest.project
  resources = local.manifest.resources
}
```

## Example from code .tf
```yaml
provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

module "module_usage" {
  source = "./../.."

  project = {
    project     = "my-project"
    environment = "dev"
    createdBy   = "markitos"
    group       = "example-usage-s3-backend"
  }

  resources = {
    bucket_name = "component-123456-abcdefg-markitos-bcket"
    table_name  = "component-123456-abcdefg-markitos-table"
    tags = {
      Name        = "TestingBucket S3"
      Description = "TestingBucket S3"
    }
  }
}
```
<!-- END_TF_DOCS -->