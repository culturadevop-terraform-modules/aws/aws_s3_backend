output "backend_bucket_id" {
  description = "Id of the created bucket s3"
  value       = aws_s3_bucket.this.id
}

output "backend_bucket_arn" {
  description = "arn of the created bucket s3"
  value       = aws_s3_bucket.this.arn
}

output "backend_bucket_name" {
  description = "name of the created bucket s3"
  value       = local.bucket_name
}

output "backend_table_name" {
  description = "name of the storage table"
  value       = aws_dynamodb_table.terraform_locks.name
}

output "backend_configuration_file_dot_env" {
  description = "generate the configuration file for the backend infrastructure"
  value       = <<EOT
TF_VAR_backend_name         = ${local.bucket_name}
TF_VAR_backend_tags         = {"Tag1":"Tag1Value"}
TF_VAR_provider_region      = "PUT_HERE_YOUR_PROVIDER_REGION"
TF_VAR_provider_profile     = "PUT_HERE_YOUR_PROVIDER_PROFILE"
TF_VAR_project_name         = ${local.project.project}
TF_VAR_project_environment  = ${local.project.environment}
TF_VAR_project_group        = ${local.project.group}
TF_VAR_project_created_by   = ${local.project.createdBy}
TF_TOKEN_gitlab_com         = "PUT_HERE_YOUR_PERSONAL_GITLAB_TOKEN"
EOT
}

output "project_backend_configuration_file_dot_env" {
  description = "generate the project configuration file for use remote backend state"
  value       = <<EOT
region              = "PUT_HERE_YOUR_PROVIDER_REGION"
profile             = "PUT_HERE_YOUR_PROVIDER_PROFILE"
bucket              = "${local.bucket_name}"
key                 = "PUT_HERE_YOUR_BACKEND_KEY"
dynamodb_table      = "${aws_dynamodb_table.terraform_locks.name}"
encrypt             = true
TF_TOKEN_gitlab_com = "PUT_HERE_YOUR_PERSONAL_GITLAB_TOKEN"
EOT
}


output "project_configuration_file_dot_env" {
  description = "generate the project configuration file .env"
  value       = <<EOT
TF_VAR_project        = "my-project"
TF_VAR_environment    = "dev"
TF_VAR_createdBy      = "markitos"
TF_VAR_group          = "my-group"
TF_VAR_region         = "PUT_HERE_YOUR_PROVIDER_REGION"
TF_VAR_profile        = "PUT_HERE_YOUR_PROVIDER_PROFILE"
TF_TOKEN_gitlab_com   = "PUT_HERE_YOUR_PERSONAL_GITLAB_TOKEN"  
EOT
}


output "debug" {
  description = "For debug purpose."
  value       = local.bucket_name
}
