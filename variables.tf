# #:{'..'}>----------------------------------------------
# #:{'..'}> required variables
# #:{'..'}>----------------------------------------------
variable "resources" {
  description = "resources of objects to create"
  nullable    = false
  type = object({
    bucket_name = string
    table_name  = string
    tags        = optional(map(string))
  })
  ###### BUCKET_NAME #####
  validation {
    condition     = length(var.resources.bucket_name) >= 3 && length(var.resources.bucket_name) <= 63
    error_message = "Bucket names must be between 3 (min) and 63 (max) characters long"
  }

  validation {
    condition     = can(regex("^[a-z0-9.-]{1,64}$", var.resources.bucket_name))
    error_message = "Bucket names can consist only of lowercase letters, numbers, dots (.), and hyphens (-)."
  }

  validation {
    condition     = can(regex("^[a-z0-9].*[a-z0-9]$", var.resources.bucket_name))
    error_message = "Bucket names must begin and end with a letter or number"
  }

  validation {
    condition     = !strcontains(var.resources.bucket_name, "..")
    error_message = "Bucket names must not contain two adjacent periods."
  }

  validation {
    condition     = !can(regex("^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$", var.resources.bucket_name))
    error_message = "Bucket names must not be formatted as an IP address (for example, 192.168.5.4)"
  }

  validation {
    condition     = !startswith(var.resources.bucket_name, "xn--")
    error_message = "Bucket names must not start with the prefix xn--"
  }

  validation {
    condition     = !startswith(var.resources.bucket_name, "sthree-")
    error_message = "Bucket names must not start with the prefix sthree-"
  }

  validation {
    condition     = !endswith(var.resources.bucket_name, "-s3alias")
    error_message = "Bucket names must not end with the suffix -s3alias."
  }

  validation {
    condition     = !endswith(var.resources.bucket_name, "--ol-s3")
    error_message = "Bucket names must not end with the suffix --ol-s3"
  }
  ###### END.OF BUCKET_NAME #####


  ###### TABLE_NAME #####
  validation {
    condition     = length(var.resources.table_name) >= 3 && length(var.resources.table_name) <= 63
    error_message = "Table name must be between 3 (min) and 63 (max) characters long"
  }

  validation {
    condition     = can(regex("^[a-z0-9-]{1,64}$", var.resources.table_name))
    error_message = "Table name can consist only of lowercase letters, numbers and hyphens (-)."
  }

  validation {
    condition     = can(regex("^[a-z].*[a-z]$", var.resources.table_name))
    error_message = "Table name must begin and end with a letter"
  }

  validation {
    condition     = !strcontains(var.resources.table_name, "--")
    error_message = "Table name must not contain two adjacent hyphens (-)."
  }
  ###### END.OF TABLE_NAME #####
}


variable "project" {
  description = "some common settings like project name, environment, and tags for all resources-objects"
  nullable    = false
  type = object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
}
# #:{'..'}>----------------------------------------------



# #:{'..'}>----------------------------------------------
# #:{'..'}> optional variables
# #:{'..'}>----------------------------------------------
# #:{'..'}>----------------------------------------------
